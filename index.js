// dependencies
const AWS = require('aws-sdk');
const util = require('util');

// get reference to S3 client
const s3 = new AWS.S3();

exports.handler = async (event, context, callback) => {
    console.log("Reading options from event:\n", util.inspect(event, { depth: 5 }));
    const srcBucket = event.Records[0].s3.bucket.name;
    // Object key may have spaces or unicode non-ASCII characters.
    const srcKey = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " "));
    const dstBucket = srcBucket + "-resized";
    const dstKey = "thumbnail-" + srcKey;

    const params = {
        Bucket: dstBucket,
        Key: dstKey
    };
    console.log("params: ", params);

    try {
        console.log("deleted: ", params);
        // delete thumbnail
        await s3.deleteObject(params).promise();

    } catch (error) {
        console.log(error);
        return;
    }


};